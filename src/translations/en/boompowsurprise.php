<?php
/**
 * boompowsurprise plugin for Craft CMS 3.x
 *
 * Just a test
 *
 * @link      http://www.bigandbrown.com
 * @copyright Copyright (c) 2018 Kevin Ruiz
 */

/**
 * boompowsurprise en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('boompowsurprise', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Kevin Ruiz
 * @package   Boompowsurprise
 * @since     1.0.0
 */
return [
    'boompowsurprise plugin loaded' => 'boompowsurprise plugin loaded',
];
