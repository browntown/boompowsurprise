<?php
/**
 * boompowsurprise plugin for Craft CMS 3.x
 *
 * Just a test
 *
 * @link      http://www.bigandbrown.com
 * @copyright Copyright (c) 2018 Kevin Ruiz
 */

namespace bigandbrown\boompowsurprise\services;

use bigandbrown\boompowsurprise\Boompowsurprise;

use Craft;
use craft\base\Component;
use GuzzleHttp\Client;

/**
 * BoompowsurpriseService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Kevin Ruiz
 * @package   Boompowsurprise
 * @since     1.0.0
 */
class BoompowsurpriseService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Boompowsurprise::$plugin->boompowsurpriseService->exampleService()
     *
     * @return mixed
     */
    public function exampleService()
    {
        $result = 'something';

        return $result;
    }
    public function request($uri){
        $client = new Client();

        $pathToAPI = ($_SERVER['HTTP_HOST'] == 'ynwa.local') ? 'http://mylfccincinnati.local/api/' : 'http://204.48.29.69/api/';
        $uri = $pathToAPI.$uri;
        $header = array('headers' => array('Accept'=>'application/json', 'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjIxYzg2NGJkNjJiMWMyNDNlMDg1MDIwZDBlOTBjMTBhZjA1ZDNhOTQ1MTNjZTlmODg1YzkyNjQ3ZjZmN2Q4ZGMzY2FkZTI2MjFlNDJkMTM2In0.eyJhdWQiOiI1IiwianRpIjoiMjFjODY0YmQ2MmIxYzI0M2UwODUwMjBkMGU5MGMxMGFmMDVkM2E5NDUxM2NlOWY4ODVjOTI2NDdmNmY3ZDhkYzNjYWRlMjYyMWU0MmQxMzYiLCJpYXQiOjE1MTU0ODE0MzMsIm5iZiI6MTUxNTQ4MTQzMywiZXhwIjoxNTQ3MDE3NDMzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.VTECFWEi3pxQf4zkKSyo5ofR0P8HXs-EtpGnLbe4B9-nzou_VcUvKqs26Ch_0Ui6MYADS8LbSYheiBoS_mSQ41hYyGN2z-WI6F8d6DJTiTTtbATWgsEG8FMekXDoRhNGamlXWS7SnFL6yVPf7jblSnjKJe57COfIQLp4iWrq1CB1Sh6J4uZvGfLqDG558j6s1pfbBFmrmHXxFLJLVxQuPfvEToczy6oriX4WskzTMA8y1ASnUdnhC_1Ah8-PrAb-85v-8yEB-WCmikCi9mx5De63Opq9Dkb4yv1bHAylx1EHn8RVtZkGw-JyfRBL9vqjl7W2bHheygABMJM8_GieETJf2Hr7HYVfNJvs8cJy-dF0jt1ABGjC_14TumId_TM6hoiWUwXaev0TEJ-Q-6aGEVHSsFfJpQJc7FGHD57kbi8TQs5YzKoVW6tJiFAhJuLBvdQ1RZEBhU3_eJya4fnaASnxLhD6AK5WhydJuuWDW0P5cawVwvjYA_tC1W-ciOIbjOzaWRN-Lds7RSX2IOhrTkQykX9MDxip92z3pEp-f_sHsyoQ4n_WE149VujZN6gnqZ1yrFa1BY7EKT_rL9ApDT1AVHV1l7F-lOfvp798HWco1olep3rRjfA-k7wIKYDoDgOaLCkfUSp1uHqqHm-f5IPuV1-p6F6_Wh63rBDglhs'));
        //$response = $client->get($uri, $header)->send();
        $response = $client->request('GET', $uri, $header);
        $json = json_decode($response->getBody(), true);
        return $json;
    }
    public function leaguetable(){
        $uri = "leaguetable";
        $request = $this->request($uri);
        return $this->request($uri);
    }
    public function member($id){
        $uri = "member/".$id;
        $request = $this->request($uri);
        return $this->request($uri);
    }
    public function members(){
        $uri = "members";
        $request = $this->request($uri);
        return $this->request($uri);
    }
    public function memberRandom(){
        $uri = "memberRandom";
        $request = $this->request($uri);
        return $this->request($uri);
    }
    public function players(){
        $uri = "players";
        $request = $this->request($uri);
        return $this->request($uri);
    }
    public function qa($category_id, $user_id){
        $uri = "qa/".$category_id."/".$user_id;
        $request = $this->request($uri);
        return $this->request($uri);
    }
}
