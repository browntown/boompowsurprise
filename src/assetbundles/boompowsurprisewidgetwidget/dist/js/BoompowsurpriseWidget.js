/**
 * boompowsurprise plugin for Craft CMS
 *
 * BoompowsurpriseWidget Widget JS
 *
 * @author    Kevin Ruiz
 * @copyright Copyright (c) 2018 Kevin Ruiz
 * @link      http://www.bigandbrown.com
 * @package   Boompowsurprise
 * @since     1.0.0
 */
