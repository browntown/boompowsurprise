<?php
/**
 * boompowsurprise plugin for Craft CMS 3.x
 *
 * Just a test
 *
 * @link      http://www.bigandbrown.com
 * @copyright Copyright (c) 2018 Kevin Ruiz
 */

namespace bigandbrown\boompowsurprise\variables;

use bigandbrown\boompowsurprise\Boompowsurprise;

use Craft;

/**
 * boompowsurprise Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.boompowsurprise }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Kevin Ruiz
 * @package   Boompowsurprise
 * @since     1.0.0
 */
class BoompowsurpriseVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.boompowsurprise.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.boompowsurprise.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function exampleVariable($optional = null)
    {
        $result = "And away we go to the Twig template...";
        if ($optional) {
            $result = "I'm feeling optional today...";
        }
        return $result;
    }
    public function member($id){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->member($id);
            //return Boompowsurprise::getInstance()->mylfccincinnati_api->member($id);
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function members(){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->members();
            //return Boompowsurprise::getInstance()->boompowsurpriseService->members();
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function memberRandom(){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->memberRandom();
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function players(){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->players();
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function fixtures($id){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->fixtures($id);
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function leaguetable(){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->leaguetable();
        }
        catch(\Exception $e){
            return false;
        }
    }

    public function qa($category_id, $user_id){
        try{
            return Boompowsurprise::$plugin->boompowsurpriseService->qa($category_id, $user_id);
            //return Boompowsurprise::getInstance()->boompowsurpriseService->members();
        }
        catch(\Exception $e){
            return false;
        }
    }
}
