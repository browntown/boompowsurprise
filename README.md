# boompowsurprise plugin for Craft CMS 3.x

Just a test

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require  x/boompowsurprise

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for boompowsurprise.

## boompowsurprise Overview

-Insert text here-

## Configuring boompowsurprise

-Insert text here-

## Using boompowsurprise

-Insert text here-

## boompowsurprise Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Kevin Ruiz](http://www.bigandbrown.com)
